#include <iostream>
#include <cmath>
#include <string>
#include <vector>

#define X 10 //automatically makes a variable, aka macro constant

//Shortcut to cout stuff instead of writing it everytime like a scrub
#define LOG(y) std::cout << y << std::endl;

using std::cin;
using std::cout;

//type of declaration and helper
#pragma region typehelper

#include <type_traits>
#include <typeinfo>
#ifndef _MSC_VER
#include <cxxabi.h>
#endif
#include <memory>
#include <string>
#include <cstdlib>

template <class T>
std::string
type_name()
{
    typedef typename std::remove_reference<T>::type TR;
    std::unique_ptr<char, void (*)(void *)> own(
#ifndef _MSC_VER
        abi::__cxa_demangle(typeid(TR).name(), nullptr,
                            nullptr, nullptr),
#else
        nullptr,
#endif
        std::free);
    std::string r = own != nullptr ? own.get() : typeid(TR).name();
    if (std::is_const<TR>::value)
        r += " const";
    if (std::is_volatile<TR>::value)
        r += " volatile";
    if (std::is_lvalue_reference<T>::value)
        r += "&";
    else if (std::is_rvalue_reference<T>::value)
        r += "&&";
    return r;
}

#pragma endregion typehelper

double power(double base, int exponent)
{
    double result = 1;
    for (int i = 0; i < exponent; i++)
    {
        result = result * base;
    }
    return result;
}

void print_stuff(double base, int exponent)
{
    double myPower = power(base, exponent);
    cout << base << " raised to the " << exponent << " power is " << myPower << ".\n";
}

void sampleFunctions()
{

    auto x1 = 5U;
    auto x2 = 4.0F;
    auto x3 = 3ULL;
    auto x4 = "string";

    const std::string str1 = "j";

    cout << "x1 is " << typeid(x1).name() << std::endl;
    cout << "x2 is " << typeid(x2).name() << std::endl;
    cout << "x3 is " << typeid(x3).name() << std::endl;
    cout << "x4 is " << typeid(x4).name() << std::endl;

    //Macro Constant Example
    cout << "Macro constant defined is " << X << std::endl;

    //type check
    //Checking to see if x1 is indeed a dataType of type "j" which is Unsigneed int
    if (typeid(x1).name() == str1)
        cout << "this is a " << typeid(x4).name() << std::endl;

    double base;
    int exponent;
    cout << " what is the base? ";
    cin >> base;
    cout << "What is the exponent? ";
    cin >> exponent;
    print_stuff(base, exponent);
    print_stuff(10, 2);
    print_stuff(3.56, 9);

    // std::cout << "Hello World\n";
    // return 0;
}

void checkLoops()
{
    std::string mySequence = "1 2 3 4 5 6 7 8 9";
    for (int i = 0; i < mySequence.size(); i++)
    {
        std::cout << mySequence[i] << std::endl;
        if (mySequence[i] == '3')
        {
            std::cout << "lolz I found 3 \n"
                      << std::endl;
            continue;
        }
    }
    std::cout << "Hey Boss I'm done!\n"
              << std::endl;
}

void checkArray(int arr[], int arrsize)
{
    for (int i = 0; i < arrsize; i++)
    {
        std::cout << arr[i] << '\n';
    }
    std::cout << "here's the array boss " << '\n';
}

void checkDataType()
{
    int nums[] = {0, 2, 4, 6, 8, 10};
    int sizeOfArr = sizeof(nums) / sizeof(decltype(nums[0]));

    std::cout << "the data type checked is" << type_name<decltype(nums[0])>() << '\n';
}

void checkVector()
{
    //more information here: https://www.geeksforgeeks.org/vector-in-cpp-stl/

    //Vector is also a list, the size can be changed dynamically
    //need to include <vector> at top of script
    std::vector<int> data = {1, 2, 3};

    //This is equivalent to List.Add(element) in C#
    data.push_back(23);
    std::cout << "last element in array is " << data[data.size() - 1] << std::endl;
    //Remove last element in the array
    data.pop_back();
    std::cout << "the array size currently is " << data.size() << std::endl;
}

void ReferenceSample(int &x, int &y, int &z)
{
    //& "ampersand" will always reference the memory address of the passed in variable 
    //regardless of what they are named, this will persist outside the function call as well
    
    x = 10;
    y = 11;
    z = 12;
    int n = z;

    LOG("a in is");
    LOG(x);
    LOG("b in is ");
    LOG(y);
    LOG("c in is ");
    LOG(z);
    LOG("n in is ");
    LOG(n);
}

int main()
{
    //First 4 hours
    //sampleFunctions();
    //checkLoops();

    int sampleArray[] = {-2, -4, -6, 8, 10, 10, 20, 26};
    //To find the size of the array, use the total size of the array divided by the size of one element in the array
    int arraySize = sizeof(sampleArray) / sizeof(sampleArray[0]);
    //checkDataType();
    //checkArray(sampleArray, arraySize);
    //checkVector();

    int a = 4;
    int b = 5;
    int c = 6;

    ReferenceSample(a, b, c);

    LOG("a out is");
    LOG(a);
    LOG("b out is ");
    LOG(b);
    LOG("c out is ");
    LOG(c);
}