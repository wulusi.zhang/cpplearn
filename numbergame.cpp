#include <iostream>
#include <array>
#include <vector>

//To use memset
#include <stdio.h>
#include <string.h>

//Shortcut to cout stuff instead of writing it everytime like a scrub
#define LOG(y) std::cout << y << std::endl;


//STL array example
void print_array(std::array<int, 20> &data, int size)
{

    for (int i = 0; i < size; i++)
    {
        std::cout << data[i] << "\t";
    }
    std::cout << "\n";
}

//Vector example with &data address pointer
//&data will reference last address of data vector, this results in cumulative
//processing of the vector when the vector has run multiple times
void print_vector(std::vector<int> &data)
{
    data.push_back(420);
    for (int i = 0; i < data.size(); i++)
    {
        std::cout << data[i] << "\t";
    }

    std::cout << "all done captain! "
              << "\n";
}

void MemorySetExample(){
    
    char* buffer = new char [8];
    memset(buffer, 0 , 8);

    LOG("buffer is " + *buffer);

}

int main()
{
   //For vectors do not need to specifiy the size of the vector since it will dynamically scale
    std::vector<int> vector = {0, 1, 2, 3, 4};

    int print_how_many_times = 3;

    LOG(print_how_many_times);

    for (int i = 0; i < print_how_many_times; i++)
    {
        print_vector(vector);
    }

    MemorySetExample();
    //std::array<int, 20> array = {0, 1, 2, 3};

    //print_array(array, array.size());
}
