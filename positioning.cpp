#include <iostream>
#include <cmath>
#include <string>
#include <vector>

//Add ; after class and custom overloads
class Position
{

    int privateVar1 = 5;
    std::string privateVar2 = "tanks";

public:
    int x = 10;
    int y = 20;

    std::string vectorName;

    //Operator overloading in C++
    Position operator+(Position pos)
    {
        Position new_pos;
        new_pos.x = x + pos.x;
        new_pos.y = y + pos.y;
        return new_pos;
    }

    //Rudimentary accesor
    int getprivateData()
    {
        //Since this is only returning a value here without much logic
        //There is an option to add additional computational/logic to make these methods more useful
        return privateVar1;
    }

    //Rudimentary getter
    int setprivateVar(int set)
    {
        privateVar1 = set;
        return privateVar1;
    }

    void SetPosition(int x, int y)
    {
        this->x = x;
        this->y = y;
    }

    bool operator==(Position pos)
    {
        if (x == pos.x && y == pos.y)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    //Need to define class as argument still can not directly
    //access private variables
    friend void outputPrivateVar(Position pos);

    //can also accomodate operators as well directly
    friend std::istream &operator>>(std::istream &input, Position &pos);
    friend std::ostream &operator<<(std::ostream &output, Position pos);
};

//Method can be created outside of class to fetch private variables from within

void outputPrivateVar(Position pos)
{
    std::cout << "private var is " << pos.privateVar1 << "\n";
}

std::ostream &operator<<(std::ostream &output, Position pos)
{
    output << pos.vectorName << " has "
           << "x: " << pos.x << " "
           << "y: " << pos.y << " private var is "
           << pos.privateVar1 << "\n";

    return output;
};

//Intake overload for custom names
std::istream &operator>>(std::istream &input, Position &pos)
{
    //Need to match up the data types when using cin, if the data type
    //does not match then function skips to default values and runs program
    //with these default values
    input >> pos.vectorName >> pos.x >> pos.y >> pos.privateVar1;
    return input;
};

int main()
{
    Position pos1, pos2, pos3;

    std::cin >> pos1;
    std::cin >> pos2;

    // if (pos1 == pos2)
    // {
    //     // std::cout << "pos1 is " << pos1.x << " " << pos1.y << "\n";
    //     // std::cout << "pos2 is " << pos2.x << " " << pos2.y << "\n";
    //     std::cout << "They are the same!\n";
    // }
    // else
    // {
    //     // std::cout << "pos1 is " << pos1.x << " " << pos1.y << "\n";
    //     // std::cout << "pos2 is " << pos2.x << " " << pos2.y << "\n";
    //     std::cout << "They are NOT the same!\n";
    // }

    // pos1.SetPosition(2, 3);
    // pos2.SetPosition(4, 6);

    pos3 = pos1 + pos2;
    std::cin >> pos3.vectorName;

    // if (pos1 == pos2)
    // {
    //     // std::cout << "pos1 is " << pos1.x << " " << pos1.y << "\n";
    //     // std::cout << "pos2 is " << pos2.x << " " << pos2.y << "\n";
    //     std::cout << "They are the same!\n";
    // }
    // else
    // {
    //     // std::cout << "pos1 is " << pos1.x << " " << pos1.y << "\n";
    //     // std::cout << "pos2 is " << pos2.x << " " << pos2.y << "\n";
    //     std::cout << "They are NOT the same!\n";
    // }

    std::cout << "pos3 is " << pos3 << std::endl;

    std::cout << pos1 << "\n";
    std::cout << pos2 << "\n";
    std::cout << pos3 << "\n";

    //outputPrivateVar(pos3);

    return 0;
}