#include <iostream>
#include <cmath>
#include <string>
#include <vector>

//Overloading Function Template

//Shortcut to cout stuff instead of writing it everytime like a scrub
//#define LOG(y) std::cout << y << std::endl;

//Double overload log with 2 elements
#define LOG(x, y) std::cout << "x: " << x << "\ty:" << y << std::endl;

//Int or float swap of type T
template <typename T>
void swap(T &a, T &b)
{
    T temp = a;
    a = b;
    b = temp;
}

//Array swap
//Need to use & address pointers for operators to have actual swap occur
template <typename T>
void swap(std::vector<T> &a, std::vector<T> &b, T size)
{
    for (int i = 0; i < size; i++)
    {
        T temp = a[i];
        a[i] = b[i];
        b[i] = temp;
    }
}

//Array Swap Using Arrays actually
//See the difference in data retainment as no & is used here in the arguments
template <typename T>
void swap(T a[], T b[], T size)
{
    for (int i = 0; i < size; i++)
    {
        T temp = a[i];
        a[i] = b[i];
        b[i] = temp;
    }
}

int main()
{
    //Int swap example
    int a = 10;
    int b = 20;
    LOG(a, b);
    swap(a, b);
    LOG(a, b);

    //string swap

    std::string first_name = "Wutang";
    std::string last_name = "Zhang";

    LOG(first_name, last_name);

    swap(first_name, last_name);

    LOG(first_name, last_name);

    int eights[] = {8, 8, 8, 8, 8, 8};
    int ones[] = {1, 1, 1, 1, 1, 1};

    std::vector<int> nines = {9, 9, 9, 9, 9, 9};
    std::vector<int> six = {6, 6, 6, 6, 6, 6};

    int SIZE = nines.size();

    for (int i = 0; i < nines.size(); i++)
    {
        std::cout << nines[i] << " " << six[i] << "\t";
        std::cout << eights[i] << " " << ones[i] << "\t";
    }
    std::cout << "\n\n";

    swap(nines, six, SIZE);
    swap(eights, ones, SIZE);

    for (int i = 0; i < nines.size(); i++)
    {
        std::cout << nines[i] << " " << six[i] << "\t";
        std::cout << eights[i] << " " << ones[i] << "\t";
    }
    std::cout << "\n\n";

    return 0;
}